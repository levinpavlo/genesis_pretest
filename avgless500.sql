SELECT adverts.category_name
FROM adverts
LEFT JOIN costs on adverts.id=costs.id
GROUP BY adverts.category_name
HAVING AVG(costs.cost)<500
